'use strict';

const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const requireFromString = require('require-from-string');
const MemoryFS = require('memory-fs');

const deasync = require('deasync');

const __PROD__ = process.env.NODE_ENV === 'production';
const __DEV__ = __PROD__ === false;

const packageFile = require('./package.json');


const enviroment = packageFile.enviroments[
  (__PROD__ && '__PROD__') ||
  (__DEV__ && '__DEV__')
];

const __SSR__ = enviroment.__SSR__;
const __DEVTOOLS__ = enviroment.__DEVTOOLS__;

const define = {
  __DEV__: JSON.stringify(__DEV__),
  __PROD__: JSON.stringify(__PROD__),
  'process.env': {
    NODE_ENV: JSON.stringify(__PROD__ ? 'production' : 'development')
  },
  __SSR__: JSON.stringify(__SSR__),
  __DEVTOOLS__: JSON.stringify(__DEVTOOLS__)
};

let getServerString = undefined;
const webpackConfig = {
  devtool: __DEV__ ? 'source-map' : false,
  entry: {
    app: [
      
      './src/entry-points/Client.jsx']
  },
  output: {
    path: path.join(__dirname, 'www'),
    filename: '[name].js',
    publicPath: ''
  },
 
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name].css",
      chunkFilename: "[id].css"
    }),
    new webpack.DefinePlugin(Object.assign(define, {
      __CLIENT__: JSON.stringify(true),
      __SERVER__: JSON.stringify(false)
    })),
    new HtmlWebpackPlugin({
      minify: {},
      getAppContent: () => (__SSR__ ? '' : ''),
      template: './src/index.ejs', // Load a custom template
      inject: 'body' // Inject all scripts into the body
    })
  ],
  module: {

  

    rules: [
      {
        test: /\.exec\.js$/,
        use: [ 'script-loader' ]
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loaders: ['babel-loader']
      },
      {
        test: /\.jsx$/,
        exclude: /(node_modules|bower_components)/,
        loaders: ['babel-loader']
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader"
        ]
      },
      {
        test: /\.(png|jpg|jpeg)$/,
        loader: 'url-loader?limit=8192'
      }, {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&minetype=application/font-woff'
      }, {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file'
      }
    ]
  }
};

getServerString = () => {
  const fs = new MemoryFS();

  const compiler = webpack(Object.assign(webpackConfig, {
    entry: [
      
      './src/entry-points/Server.jsx'],
    output: {
      path: '/',
      filename: 'bundle.js',
      libraryTarget: 'umd'
    },
    module: Object.assign(webpackConfig.module, {
      loaders: webpackConfig.module.loaders.map(loaderObj => {
        let returnedLoaderObj;
        if (loaderObj.test.toString() === /\.(scss|css)$/.toString()) {
          returnedLoaderObj = Object.assign(loaderObj, {
            loader: 'css?modules&importLoaders=2&sourceMap&localIdentName=[local]___[hash:base64:5]!sass'
          });
        } else {
          returnedLoaderObj = loaderObj;
        }
        return returnedLoaderObj;
      })
    }),
    plugins: [
      new webpack.DefinePlugin(Object.assign(define, {
        __CLIENT__: JSON.stringify(false),
        __SERVER__: JSON.stringify(true)
      }))
    ]
  }));

  let sync = true;
  let data = null;
  compiler.outputFileSystem = fs;
  compiler.run(err => {
    if (err) {
      throw err;
    }
  });

  while (sync) {
    deasync.sleep(100);
  }

  return data;
};

module.exports = webpackConfig;
