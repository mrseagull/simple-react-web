

import mainStore from './stores/mainStore'

import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/App';

ReactDOM.render(

  <App store={mainStore}/>,
  document.getElementById('root')
);
