import React, { Component } from 'react';


import _ from 'lodash';
import 'jodit';
import 'jodit/build/jodit.min.css';
import JoditEditor from "jodit-react";


import '../css/app.css';


export class App extends Component {

  constructor(props) {
      super(props);
      this.state = {
          selectIndex: 0,
          title: '',
          table: '',
          selectIndex: 0,
          addMode: false,
          editMode: false,
          cols: [],
          data: [],
          tables: {},
          editForm: {},
          addForm: {},
          search: '',
          fastEdit: {},
          editorState:"",

      };
  }

  getTable(table) {
      let formData = new FormData();
      formData.append('table', table);
      formData.append('action', 'get');
      fetch(window.SERVER_NAME+'/api/admin',
          {
              method: 'post',
              body: formData
          }).then((reponse) => reponse.json())
          .then((json) => {
              this.setState({ ...this.state, tables: { ...this.state.tables, [table]: json.data } });
              //console.log(json);
          });

  }

  getData() {
      //console.log('getData', window.mamaData[this.state.selectIndex].table);

      //Загрузка смежный таблиц
      window.mamaData[this.state.selectIndex].cols.forEach((item) => {

          if (item.type == 'table') {
              //console.log(item);
              this.getTable(item.table.name);
          }
      });

      let formData = new FormData();
      formData.append('table', window.mamaData[this.state.selectIndex].table);
      formData.append('action', 'get');
      fetch(window.SERVER_NAME+'/api/admin',
          {
              method: 'post',
              body: formData
          }).then((reponse) => reponse.json())
          .then((json) => {
              this.setState({ ...this.state, data: json.data, addMode: false, editMode: false, fastEdit: {} });
              //console.log(json);
          });
  }

  setTabel(index) {
      this.setState({ ...this.state, selectIndex: index, cols: window.mamaData[index].cols,  addMode: false, editMode: false, search:'' }, () => {
          this.getData();
      });

  }
  componentDidMount() {
    if(!window.SERVER_NAME) window.SERVER_NAME = '';
      this.setTabel(0);
     
  }

  getName(item) {
      let name = '';
      _.map(item, (val) => {
          if (val.length < 10)
              name = name + val + ' ';
      });
      return name;
  }

  getMultiSelectValue(values, value){
      let tmp = false;
      values.split('|').forEach((item)=>{
        if(item == value) tmp = true;
      });

      return tmp;

  }

  getInput(item) {

      let tabel = '';
      let action = item.type;
      if (action.indexOf('*') > -1) {
          let tmp = action.split('*');
          action = tmp[0];
          tabel = tmp[1];

          //console.log('!!!!!tabel', tabel);
      }



      switch (action) {
          case 'text':
              return <div className="form-group">
                  <label className="col-sm-3 control-label">{item.title}</label>
                  <div className="col-sm-12">
                      <input value={this.state.addForm[item.name] && this.state.addForm[item.name]} onChange={(e) => { let addForm = this.state.addForm; addForm[item.name] = e.target.value; this.setState({ ...this.state, addForm: addForm }); }} type="text" className="form-control" name="{item.name}" id="{item.name}" placeholder="" />
                  </div>
              </div>
              break;

          case 'textarea':
              return <div className="form-group">
                  <label className="col-sm-3 control-label">{item.title}</label>
                  <div className="col-sm-12">
                      <textarea value={this.state.addForm[item.name] && this.state.addForm[item.name]} onChange={(e) => { let addForm = this.state.addForm; addForm[item.name] = e.target.value; this.setState({ ...this.state, addForm: addForm }); }} name="{item.name}" id="{item.name}" className="form-control"></textarea>
                  </div>
              </div>
              break;

          case 'bool':
              return <div className="form-group">
                  <label className="col-sm-3 control-label">{item.title}</label>
                  <div className="col-sm-12">
                      <input onChange={(e) => { let addForm = this.state.addForm; addForm[item.name] = e.target.checked ? 1 : 0; this.setState({ ...this.state, addForm: addForm }); }} name="{item.name}" type="checkbox" data-on-color="primary" className="input-switch" />
                  </div>
              </div>
              break;
          case 'number':
              return <div className="form-group">
                  <label className="col-sm-3 control-label">{item.title}</label>
                  <div className="col-sm-12">
                      <input value={this.state.addForm[item.name] && this.state.addForm[item.name]} onChange={(e) => { let addForm = this.state.addForm; addForm[item.name] = e.target.value; this.setState({ ...this.state, addForm: addForm }); }} type="number" className="form-control" name="{item.name}" id="{item.name}" placeholder="" />
                  </div>
              </div>
              break;

          case 'image':
              return <div className="form-group">

                  <label className="col-sm-3 control-label">{item.title}</label>
                  <div className={"col-sm-12"}>
                      <div className="form-group">
                          <label for="exampleFormControlFile1">{this.state.addForm[item.name] && this.state.addForm[item.name].name}</label>
                          <input type="file" className="form-control-file" onChange={(e) => { this.setState({ ...this.state, addForm: { ...this.state.addForm, [item.name]: e.target.files[0] } }); }} name="{item.name}" />
                      </div>

                  </div>
              </div>



              break;


          case 'select':


              return <div className="form-group">
                  <label className="col-sm-3 control-label">{item.title}</label>
                  <div className={"col-sm-12"}>
                      <select onChange={(e) => { this.setState({ ...this.state, addForm: { ...this.state.addForm, [item.name]: e.target.value } }) }} name={item.name} className="custom-select">
                          {item.values.map((itemV) => {
                              return <option value={itemV.id} selected={this.state.addForm[item.name] == itemV.id ? true : false}>{itemV.value}</option>
                          })}
                      </select>
                  </div>

              </div>

              break;
            case 'multiselect':

            return <div className="form-group">
            <label className="col-sm-3 control-label">{item.title}</label>
            <div className={"col-sm-12"}>
                <select onChange={(e) => { this.setState({ ...this.state, addForm: { ...this.state.addForm, [item.name]: e.target.value } }) }} name={item.name} className="custom-select">
                    {item.values.map((itemV) => {
                        return <div className="form-check">
                        <input className="form-check-input" type="checkbox"/>
                        <label className="form-check-label" for="gridCheck1">
                          Example checkbox
                        </label>
                      </div> 
                      
                       <option value={itemV.id} selected={this.getMultiSelectValue(this.state.addForm[item.name],itemV.id)}>{itemV.value}</option>
                    })}
                </select>
            </div>

        </div>


            break;
            
      
              case 'html':
          
              return <div className="form-group">
              <label className="col-sm-3 control-label">{item.title}</label>
              <div className={"col-sm-12"}>
                  <JoditEditor 
                       value={this.state.addForm[item.name] && this.state.addForm[item.name]} 
                        onChange={this.updateContent}
                       onChange={(value) => { let addForm = this.state.addForm; addForm[item.name] = value; this.setState({ ...this.state, addForm: addForm }); }} />
              </div>
          </div>
              break;

              

          case 'table':


              return <div className="form-group">
                  <label className="col-sm-3 control-label">{item.title}</label>
                  <div className={"col-sm-12"}>
                      <select onChange={(e) => { this.setState({ ...this.state, addForm: { ...this.state.addForm, [item.name]: e.target.value } }) }} name={item.name} className="custom-select">
                      <option value="0" selected={this.state.addForm[item.name] == 0 ? true : false}>Не выбранно</option>
                          {this.state.tables[item.table.name] && this.state.tables[item.table.name].map((itemV) => {
                              return <option value={itemV[item.table.key]} selected={this.state.addForm[item.name] == itemV[item.table.key] ? true : false}>{itemV[item.table.value]}</option>
                          })}
                      </select>
                  </div>

              </div>

              break;




          default:

              break;
      }




  }


  RemoveHTMLTags(html) {
    var regX = /(<([^>]+)>)/ig;                
    return(html.replace(regX, ""));
}


  onSearch(item) {

      if (!this.state.search) return true;
      else {
          let line = "";
          _.map(item, (val) => {
              line = line + val + ' ';
          });

          if (line.toLowerCase().indexOf(this.state.search.toLowerCase()) > -1) return true;
          else return false;
      }
  }

  onCloseEdit(item) {
      this.setState({ ...this.state, fastEdit: {} });
  }

  onSaveEdit(item) {
      this.onUpdate();
  }

  getEditInput(item, mini) {


      let tabel = '';
      let action = item.type;
      if (action.indexOf('*') > -1) {
          let tmp = action.split('*');
          action = tmp[0];
          tabel = tmp[1];

          //console.log('!!!!!tabel', tabel);
      }


      switch (action) {
          case 'text':
              return <div className="form-group">
                  {!mini && <label className="col-sm-3 control-label">{item.title}</label>}
                  <div className={"col-sm-12"}>
                      <input value={this.state.editForm[item.name] && this.state.editForm[item.name]} onChange={(e) => { let editForm = this.state.editForm; editForm[item.name] = e.target.value; this.setState({ ...this.state, editForm: editForm }); }} type="text" className="form-control" name="{item.name}" id="{item.name}" placeholder="" />
                  </div>


                  {mini && <div style={{ display: 'flex', justifyContent: 'center', marginTop: 10 }}> <button className={"col-sm-12"} onClick={() => this.onSaveEdit(item)} className="btn btn-round btn-success" style={{ marginRight: 10 }}>
                      Схр.
                             </button>
                      <button onClick={() => this.onCloseEdit(item)} className="btn btn-round btn-primary">
                          Отм.
                   </button></div>}

              </div>
              break;

          case 'textarea':
              return <div className="form-group">
                  {!mini && <label className="col-sm-3 control-label">{item.title}</label>}
                  <div className={"col-sm-12"}>
                      <textarea value={this.state.editForm[item.name] && this.state.editForm[item.name]} onChange={(e) => { let editForm = this.state.editForm; editForm[item.name] = e.target.value; this.setState({ ...this.state, editForm: editForm }); }} name="{item.name}" id="{item.name}" className="form-control"></textarea>
                  </div>
                  {mini && <div style={{ display: 'flex', justifyContent: 'center', marginTop: 10 }}> <button className={"col-sm-12"} onClick={() => this.onSaveEdit(item)} className="btn btn-round btn-success" style={{ marginRight: 10 }}>
                      Сохр.
                             </button>
                      <button onClick={() => this.onCloseEdit(item)} className="btn btn-round btn-primary">
                          Отм.
                   </button></div>}
              </div>
              break;

          case 'bool':
              return <div className="form-group">
                  {!mini && <label className="col-sm-3 control-label">{item.title}</label>}
                  <div className={"col-sm-12"}>
                      <input checked={this.state.editForm[item.name] == 1 ? true : false} onChange={(e) => { let editForm = this.state.editForm; editForm[item.name] = e.target.checked ? 1 : 0; this.setState({ ...this.state, editForm: editForm }); }} name="{item.name}" type="checkbox" data-on-color="primary" className="input-switch" />
                  </div>
                  {mini && <div style={{ display: 'flex', justifyContent: 'center', marginTop: 10 }}> <button className={"col-sm-12"} onClick={() => this.onSaveEdit(item)} className="btn btn-round btn-success" style={{ marginRight: 10 }}>
                      Сохр.
                             </button>
                      <button onClick={() => this.onCloseEdit(item)} className="btn btn-round btn-primary">
                          Отм.
                   </button></div>}
              </div>
              break;
          case 'number':
              return <div className="form-group">
                  {!mini && <label className="col-sm-3 control-label">{item.title}</label>}
                  <div className={"col-sm-12"}>
                      <input value={this.state.editForm[item.name] && this.state.editForm[item.name]} onChange={(e) => { let editForm = this.state.editForm; editForm[item.name] = e.target.value; this.setState({ ...this.state, editForm: editForm }); }} type="number" className="form-control" name="{item.name}" id="{item.name}" placeholder="" />
                  </div>
                  {mini && <div style={{ display: 'flex', justifyContent: 'center', marginTop: 10 }}> <button className={"col-sm-12"} onClick={() => this.onSaveEdit(item)} className="btn btn-round btn-success" style={{ marginRight: 10 }}>
                      Сохр.
                             </button>
                      <button onClick={() => this.onCloseEdit(item)} className="btn btn-round btn-primary">
                          Отм.
                   </button></div>}
              </div>
              break;

          case 'image':
              return <div className="form-group">
                  {this.state.editForm[item.name].length > 0 && <div className="col-sm-12" style={{
                      height: 150, marginBottom: 20, backgroundImage: "url(" + (this.state.editForm[item.name]) + ")", backgroundPosition: 'center',
                      backgroundSize: 'contain', backgroundRepeat: 'no-repeat'
                  }}>
                  </div>}

                  {!mini && <label className="col-sm-3 control-label">{item.title}</label>}

                  <div className={"col-sm-12"}>
                      <div className="form-group">
                          <label for="exampleFormControlFile1">{this.state.editForm[item.name] && this.state.editForm[item.name].name}</label>
                          <input type="file" className="form-control-file" onChange={(e) => { this.setState({ ...this.state, editForm: { ...this.state.editForm, [item.name]: e.target.files[0] } }); }} name="{item.name}" />
                      </div>

                  </div>
              </div>

              break;
        

          case 'html':
          
          return <div className="form-group">
          {!mini && <label className="col-sm-3 control-label">{item.title}</label>}
          <div className={"col-sm-12"}>
              <JoditEditor 
                   value={this.state.editForm[item.name] && this.state.editForm[item.name]} 
                    onChange={this.updateContent}
                   onChange={(value) => { let editForm = this.state.editForm; editForm[item.name] = value; this.setState({ ...this.state, editForm: editForm }); }} />
          </div>
          {mini && <div style={{ display: 'flex', justifyContent: 'center', marginTop: 10 }}> <button className={"col-sm-12"} onClick={() => this.onSaveEdit(item)} className="btn btn-round btn-success" style={{ marginRight: 10 }}>
              Сохр.
                     </button>
              <button onClick={() => this.onCloseEdit(item)} className="btn btn-round btn-primary">
                  Отм.
           </button></div>}
      </div>
          break;

          case 'select':


              return <div className="form-group">
                  {!mini && <label className="col-sm-3 control-label">{item.title}</label>}
                  <div className={"col-sm-12"}>
                      <select onChange={(e) => { this.setState({ ...this.state, editForm: { ...this.state.editForm, [item.name]: e.target.value } }) }} name={item.name} className="custom-select">
                          {item.values.map((itemV) => {
                              return <option value={itemV.id} selected={this.state.editForm[item.name] == itemV.id ? true : false}>{itemV.value}</option>
                          })}
                      </select>
                  </div>
                  {mini && <div style={{ display: 'flex', justifyContent: 'center', marginTop: 10 }}> <button className={"col-sm-12"} onClick={() => this.onSaveEdit(item)} className="btn btn-round btn-success" style={{ marginRight: 10 }}>
                      Сохр.
                             </button>
                      <button onClick={() => this.onCloseEdit(item)} className="btn btn-round btn-primary">
                          Отм.
                   </button></div>}
              </div>

              break;


          case 'table':


              return <div className="form-group">
                  <label className="col-sm-3 control-label">{item.title}</label>
                  <div className={"col-sm-12"}>
                      <select onChange={(e) => { this.setState({ ...this.state, editForm: { ...this.state.editForm, [item.name]: e.target.value } }) }} name={item.name} className="custom-select">
                         <option value="0" selected={this.state.editForm[item.name] == 0 ? true : false}>Не выбранно</option>
                          {this.state.tables[item.table.name] && this.state.tables[item.table.name].map((itemV) => {
                              return <option value={itemV[item.table.key]} selected={this.state.editForm[item.name] == itemV[item.table.key] ? true : false}>{itemV[item.table.value]}</option>
                          })}
                      </select>
                  </div>

                  {mini && <div style={{ display: 'flex', justifyContent: 'center', marginTop: 10 }}> <button className={"col-sm-12"} onClick={() => this.onSaveEdit(item)} className="btn btn-round btn-success" style={{ marginRight: 10 }}>
                      Сохр.
                             </button>
                      <button onClick={() => this.onCloseEdit(item)} className="btn btn-round btn-primary">
                          Отм.
                   </button></div>}
              </div>

              break;


          default:

              break;
      }






  }

  getTypeCol(name) {
      let response = false;
      this.state.cols.forEach((item) => {
          if (item.name == name)
              response = item;
      });

      return response;
  }

  onEdit(item) {
      this.setState({ ...this.state, editForm: item, editMode: true, fastEdit: {} });
  }

  onUpdate() {

      if (!window.mamaData[this.state.selectIndex].table) return;
      let formData = new FormData();

      for (let key in this.state.editForm) {
          formData.append(key, this.state.editForm[key]);
          console.log(key, this.state.editForm[key]);
      }


      formData.append('table', window.mamaData[this.state.selectIndex].table);
      formData.append('action', 'update');
      fetch(window.SERVER_NAME+'/api/admin',
          {
              method: 'post',
              body: formData
          }).then((reponse) => reponse.json())
          .then((json) => {
              //console.log(json);
              this.getData();
          });

  }

  onAdd() {



      let formData = new FormData();

      for (let key in this.state.addForm) {
          formData.append(key, this.state.addForm[key]);
      }


      formData.append('table', window.mamaData[this.state.selectIndex].table);
      formData.append('action', 'add');
      fetch(window.SERVER_NAME+'/api/admin',
          {
              method: 'post',
              body: formData
          }).then((reponse) => reponse.json())
          .then((json) => {
              //console.log(json);
              this.getData();
          });

  }

  fastEdit(item, key, row) {

      this.setState({
          ...this.state, fastEdit: {
              id: item.id,
              key: key
          }, editForm: item
      });
      //console.log(this.state.fastEdit);
  }

  onRemove(item) {
      if (!item.id) return;
      if (window.confirm("Вы действительно хотите удалить эту позицию?")) {



          if (!window.mamaData[this.state.selectIndex].table) return;
          let formData = new FormData();



          formData.append('id', item.id);
          formData.append('table', window.mamaData[this.state.selectIndex].table);
          formData.append('action', 'remove');
          fetch(window.SERVER_NAME+'/api/admin',
              {
                  method: 'post',
                  body: formData
              }).then((reponse) => reponse.json())
              .then((json) => {
                  //console.log(json);
                  this.getData();
              });
      }
  }

  getRow(item) {
      //console.log(item);
      return <React.Fragment>
          {this.state.cols.map((itemCol) => {

              return _.map(item, (row, key) => {
                  if (key == itemCol.name) {
                      let itemCol = this.getTypeCol(key);
                      if (itemCol.type == 'image') {
                          return <td><div style={{
                              height: 100, width: 100, backgroundImage: "url(" + row + ")", backgroundPosition: 'center',
                              backgroundSize: 'contain', backgroundRepeat: 'no-repeat'
                          }} /></td>
                      } else if (itemCol.type == 'bool') {
                          return <td onDoubleClick={() => this.fastEdit(item, key, row)}>
                              {(this.state.fastEdit.id == item.id && key == this.state.fastEdit.key) ? this.getEditInput(itemCol, true) : (row == 1 ? 'Да' : 'Нет')}
                          </td>
                      } else if (itemCol.type == 'select') {
                          return <td onDoubleClick={() => this.fastEdit(item, key, row)}>
                              {(this.state.fastEdit.id == item.id && key == this.state.fastEdit.key) ? this.getEditInput(itemCol, true) : <span>{itemCol.values.map((item) => {
                                  if (item.id == row) return item.value;
                              })}</span>}
                          </td>
                      } else if (itemCol.type == 'table') {
                          return <td onDoubleClick={() => this.fastEdit(item, key, row)}>
                              {(this.state.fastEdit.id == item.id && key == this.state.fastEdit.key) ? this.getEditInput(itemCol, true) : <span>{this.state.tables[itemCol.table.name] && this.state.tables[itemCol.table.name].map((item) => {
                                  if (item[itemCol.table.key] == row) return item[itemCol.table.value]
                              })}</span>}
                          </td>
                      }else if(itemCol.type == 'multiselect'){
                        return <td onDoubleClick={() => this.fastEdit(item, key, row)}>
                        {(this.state.fastEdit.id == item.id && key == this.state.fastEdit.key) ? this.getEditInput(itemCol, true) : <span>{itemCol.values.map((item) => {
                            row.span('|').map((rowItem)=>{
                                if (item.id == rowItem) return item.value;
                            })
                        })}</span>}
                       </td>
                      }else if(itemCol.type == 'html'){
                        return <td onDoubleClick={() => this.fastEdit(item, key, row)}>
                        {(this.state.fastEdit.id == item.id && key == this.state.fastEdit.key) ? this.getEditInput(itemCol, true) : this.RemoveHTMLTags(row)}
                         </td>
                      } else if (itemCol.name) {
                          return <td onDoubleClick={() => this.fastEdit(item, key, row)}>
                              {(this.state.fastEdit.id == item.id && key == this.state.fastEdit.key) ? this.getEditInput(itemCol, true) : row}
                          </td>
                      }
                  }
              })

          })
          }
          <td>


              <button onClick={() => this.onEdit(item)} type="button" className="btn btn-secondary  btn-sm btn-success" style={{ marginRight: 10 }}>
                  Ред.
                                     </button>
            {(!window.mamaData[this.state.selectIndex].locked || !window.mamaData[this.state.selectIndex].locked.remove) && <button onClick={() => this.onRemove(item)} type="button" className="btn btn-secondary  btn-sm btn btn-round btn-primary">
                  Удл.
                                     </button>}
          </td>
      </React.Fragment>

  }

  render() {

      return (


          <div className="container-fluid">


              <div className="row">
                  <nav className="col-md-2 d-none d-md-block bg-light sidebar">
                      <div className="sidebar-sticky">
                          <ul className="nav flex-column">

                              {window.mamaData.map((item, index) => {
                                  return <li className="nav-item">
                                      <a className={"nav-link" + (this.state.selectIndex == index ? " active" : "")} onClick={() => this.setTabel(index)}>
                                          <span data-feather={item.icon || 'arrow-right'}></span>
                                          {item.title}
                                      </a>
                                  </li>
                              })}


                          </ul>
                      </div>
                  </nav>

                  <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">


    

    

                      {!this.state.editMode && !this.state.addMode && <div>
                          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                              <h1 className="h2">{window.mamaData[this.state.selectIndex].title || ''}</h1>
                              <div className="btn-toolbar mb-2 mb-md-0">
                                  <div className="btn-group mr-2">
                                    {(!window.mamaData[this.state.selectIndex].locked || !window.mamaData[this.state.selectIndex].locked.add) &&  <button onClick={() => this.setState({ ...this.state, addMode: true })} className="btn btn-sm btn-success">Добавить</button>}

                                  </div>
                              </div>
                          </div>

                          <nav className="navbar navbar-dark  bg-dark flex-md-nowrap p-0 shadow">
                              <input className="form-control form-control-dark w-100" value={this.state.search} onChange={(e) => this.setState({ ...this.state, search: e.target.value })} type="text" placeholder="Поиск" aria-label="Поиск" />

                          </nav>
                          <hr /></div>}


                      {this.state.addMode && <div>

                          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                              <h1 className="h2">Добавить</h1>
                              <div className="btn-toolbar mb-2 mb-md-0">
                                  <div className="btn-group mr-2">
                                      <button onClick={() => this.onAdd()} className="btn btn-sm  btn-success">Добавить</button>

                                      <button onClick={() => this.setState({ ...this.state, addMode: false })} className="btn btn-sm btn-danger">Закрыть</button>
                                  </div>
                              </div>
                          </div>


                          <div className="example-box-wrapper">
                              <div className="form-horizontal bordered-row">
                                  {
                                      this.state.cols.map((item) => {
                                          return this.getInput(item);
                                      })
                                  }

                              </div>




                          </div>

                      </div>}

                      {this.state.editMode && <div className="panel">
                          <div>

                              <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                                  <h1 className="h2">Редактировать</h1>
                                  <div className="btn-toolbar mb-2 mb-md-0">
                                      <div className="btn-group mr-2">
                                          <button onClick={() => this.onUpdate()} className="btn btn-sm btn-success">Сохранить</button>

                                          <button onClick={() => this.setState({ ...this.state, editMode: false })} type="button" className="btn btn-sm btn-danger">Закрыть</button>
                                      </div>
                                  </div>
                              </div>



                              <div className="example-box-wrapper">
                                  <div className="form-horizontal bordered-row">
                                      {
                                          this.state.cols.map((item) => {
                                              return this.getEditInput(item);
                                          })
                                      }

                                  </div>

                              </div>
                          </div>
                      </div>}




                      {!this.state.editMode && !this.state.addMode && <div className="panel-body">
                          <div className="table-responsive">
                              <table className="table table-striped table-sm">
                                  <thead>
                                      <tr>
                                          {this.state.cols.map((item) => {
                                              let width = 100/this.state.cols.length;
                                              return <th style={{width:width+'%', fontSize:10, textAlign:'center'}}>{item.title}</th>
                                          })}
                                          <th />
                                      </tr>

                                  </thead>
                                  <tbody>

                                      {this.state.data.map((item) => {
                                          if (this.onSearch(item))
                                              return (<tr>{this.getRow(item)}</tr>);
                                      })}

                                  </tbody>
                              </table>
                          </div>
                      </div>}




                  </main>
              </div>
          </div>


      );
  }
}
