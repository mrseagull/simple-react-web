import React from 'react';
import ReactDOM from 'react-dom';

import mainStore from '../stores/mainStore';
import {App} from '../components/App';



module.exports = ReactDOMServer.renderToString(
  <App store={mainStore}/>
);
